import { FirebaseService } from "@/services/Firebase.service";

export const ValuesModuleActions = {
  FetchUserValues: "ValuesModule/fetchUserValues",
  FetchCoreValues: "ValuesModule/fetchCoreValues",
  SaveUserValues: "ValuesModule/saveUserValues",
  FetchAlignmentTrend: "ValuesModule/fetchAlignmentTrend",
};

export const ValuesModuleGetters = {
  UserValues: "ValuesModule/userValues",
  CoreUserValues: "ValuesModule/coreUserValues",
  RecentAlignment: "ValuesModule/recentAlignment",
  AverageAlignment: "ValuesModule/averageAlignment",
};

export const ValuesModule = {
  namespaced: true,
  state: {
    userValues: [],
    coreValues: [],
    alignmentTrend: [],
  },
  mutations: {
    setUserValues(state, values) {
      state.userValues = [...values];
    },
    setCoreValues(state, values) {
      state.coreValues = values;
    },
    setAlignmentTrend(state, alignmentTrend) {
      state.alignmentTrend = alignmentTrend;
    },
  },
  actions: {
    fetchUserValues({ commit }, userUid) {
      FirebaseService.fetchUserValues(userUid)
        .then((values) => {
          commit("setUserValues", values);
        })
        .catch(() => {
          commit("setUserValues", []);
        });
    },
    fetchCoreValues({ commit }, userUid) {
      return new Promise((resolve, reject) => {
        FirebaseService.fetchCoreValues(userUid)
          .then((values) => {
            commit("setCoreValues", values);
            resolve(values);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    fetchAlignmentTrend({ commit }, userUid) {
      return new Promise((resolve, reject) => {
        FirebaseService.fetchAlignmentTrend(userUid)
          .then((alignmentTrend) => {
            commit("setAlignmentTrend", alignmentTrend);
            resolve(alignmentTrend);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    saveUserValues({ commit }, { uid, values, collection }) {
      commit("setUserValues", values);
      return FirebaseService.saveUserValues(uid, values, collection);
    },
  },
  getters: {
    userValues({ userValues }) {
      return userValues;
    },
    coreUserValues({ coreValues }) {
      return coreValues;
    },
    recentAlignment({ alignmentTrend }) {
      if (!alignmentTrend.length) {
        return [];
      }

      return [...alignmentTrend[0]]
        .map((alignment) => ({
          ...alignment,
          alignment: alignment.alignment / 3,
        }))
        .sort((a, b) => {
          return a.alignment > b.alignment ? -1 : 1;
        });
    },
    averageAlignment({ alignmentTrend }) {
      const values = [];
      const valuesMap = {};

      console.log(alignmentTrend);

      alignmentTrend.forEach((alignmentTrendSet, index) => {
        alignmentTrendSet.forEach((alignmentSet) => {
          if (index === 0) {
            valuesMap[alignmentSet.title] = {
              counts: 1,
              alignment: alignmentSet.alignment,
            };
          } else {
            if (valuesMap[alignmentSet.title]) {
              valuesMap[alignmentSet.title].counts++;
              valuesMap[alignmentSet.title].alignment += alignmentSet.alignment;
            }
          }
        });
      });

      Object.keys(valuesMap).forEach((key) => {
        values.push({
          title: key,
          counts: valuesMap[key].counts,
          alignment: valuesMap[key].alignment / (valuesMap[key].counts * 3),
        });
      });

      values.sort((a, b) => {
        return a.alignment > b.alignment ? -1 : 1;
      });
      return values;
    },
  },
};
