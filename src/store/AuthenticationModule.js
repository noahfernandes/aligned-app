import { FirebaseService } from "@/services/Firebase.service";

export const AuthenticationModuleActions = {
  LoginWithGoogle: "AuthenticationModule/loginWithGoogle",
  setUser: "AuthenticationModule/setUser",
  logoutUser: "AuthenticationModule/logoutUser",
};

export const AuthenticationModuleGetters = {
  User: "AuthenticationModule/user",
};

export const AuthenticationModule = {
  namespaced: true,
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    loginWithGoogle({ commit, dispatch }) {
      FirebaseService.loginWithGoogle()
        .then((userCredential) => {
          dispatch("setUser", userCredential.user);
        })
        .catch(() => {
          commit("setUser", null);
        });
    },
    setUser({ commit }, user) {
      if (!user) {
        commit("setUser", null);
      } else {
        const { displayName, email, uid, photoURL } = user;
        commit("setUser", { displayName, email, uid, photoURL });
      }
    },
    logoutUser({ commit }) {
      FirebaseService.logoutUser().then(() => {
        commit("setUser", null);
      });
    },
  },
  getters: {
    user({ user }) {
      return user;
    },
  },
};
