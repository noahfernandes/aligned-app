import { createStore } from "vuex";
import { AuthenticationModule } from "@/store/AuthenticationModule";
import { ValuesModule } from "@/store/ValuesModule";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    AuthenticationModule,
    ValuesModule,
  },
});
