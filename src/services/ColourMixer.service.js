export class ColourMixerService {
  static decimalToHex(decimal) {
    return decimal.toString(16);
  }

  static hexToDecimal(hex) {
    return parseInt(hex, 16);
  }

  static mix(colour1, colour2, weight = 50) {
    let color = "#";

    // loop through each of the 3 hex pairs: red, green, and blue
    for (let i = 0; i <= 5; i += 2) {
      const pair1 = ColourMixerService.hexToDecimal(colour1.substr(i, 2));
      const pair2 = ColourMixerService.hexToDecimal(colour2.substr(i, 2));
      let mixed = ColourMixerService.decimalToHex(
        Math.floor(pair1 + (pair2 - pair1) * (weight / 100.0))
      );

      while (mixed.length < 2) {
        mixed = "0" + mixed;
      }

      color += mixed;
    }

    return color;
  }
}
