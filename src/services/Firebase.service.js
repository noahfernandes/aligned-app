import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyANlol5obr0_WPxEGsxazfI_a0voV1Zhmg",
  authDomain: "aligned-app-1c1b6.firebaseapp.com",
  databaseURL: "https://aligned-app-1c1b6.firebaseio.com",
  projectId: "aligned-app-1c1b6",
  storageBucket: "aligned-app-1c1b6.appspot.com",
  messagingSenderId: "710089786965",
  appId: "1:710089786965:web:81d7496e42c4149c1a36dc",
  measurementId: "G-B1TYQRJS26",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const UserCollection = firebase.firestore().collection("users");
export const GoogleProvider = new firebase.auth.GoogleAuthProvider().setCustomParameters(
  { prompt: "select_account" }
);

export class FirebaseService {
  static loginWithGoogle() {
    return firebase.auth().signInWithPopup(GoogleProvider);
  }

  static onAuthenticationChange(callback) {
    firebase.auth().onAuthStateChanged(callback);
  }

  static logoutUser() {
    return firebase.auth().signOut();
  }

  static fetchUserValues(uid) {
    return new Promise((resolve, reject) => {
      UserCollection.doc(uid)
        .get()
        .then((coreValuesDoc) => {
          const values = coreValuesDoc.exists
            ? coreValuesDoc.data().values
            : [];
          resolve(values);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  static fetchCoreValues(uid) {
    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection(`users`)
        .doc(uid)
        .collection("coreValues")
        .orderBy("timestamp", "desc")
        .limit(1)
        .get()
        .then((querySnapshot) => {
          const values = [];
          querySnapshot.forEach((docSnapshot) => {
            docSnapshot.data().values.forEach((value) => {
              // if (value.importance === 3) {
              values.push(value);
              // }
            });
          });
          resolve(values);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  static fetchAlignmentTrend(uid) {
    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection(`users`)
        .doc(uid)
        .collection("alignedValues")
        .orderBy("timestamp", "desc")
        .limit(100)
        .get()
        .then((querySnapshot) => {
          const values = [];
          querySnapshot.forEach((docSnapshot) => {
            values.push(docSnapshot.data().values);
          });
          resolve(values);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  static saveUserValues(uid, values, collection) {
    return firebase.firestore().collection(`users/${uid}/${collection}`).add({
      values,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
  }
}
