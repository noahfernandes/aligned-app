export default [
  {
    title: "Boldness",
    description:
      "Being willing to undertake things that involve risk or danger",
  },
  {
    title: "Hard work",
    description: "To work hard and well at my life tasks",
  },
  {
    title: "Self-Control",
    description: "To be disciplined in my own actions",
  },
  {
    title: "Honesty",
    description: "To be honest and truthful",
  },
  {
    title: "Autonomy",
    description: "To be self-determined and independent",
  },
  {
    title: "Challenge",
    description: "To take on difficult tasks and problems",
  },
  {
    title: "Virtue",
    description: "To live a morally pure and excellent life",
  },
  {
    title: "Dependability",
    description: "To be reliable and trustworthy",
  },
  {
    title: "Adventure",
    description: "To have new and exciting experiences",
  },
  {
    title: "Openness",
    description: "To be open to new experiences, ideas, and options",
  },
  {
    title: "Beauty",
    description: "To create and/or appreciate beauty around me",
  },
  {
    title: "Health",
    description: "To be physically well and healthy",
  },
  {
    title: "Creativity",
    description: "To have new and original ideas",
  },
  {
    title: "Growth",
    description: "To keep changing and growing",
  },
  {
    title: "Ecology",
    description: "To live in harmony with the environment",
  },
  {
    title: "Humor",
    description: "To see the humorous side of myself and the world",
  },
  {
    title: "Knowledge",
    description: "To learn and contribute valuable knowledge",
  },
  {
    title: "Passion",
    description: "To have deep feelings about ideas, activities, and people",
  },
  {
    title: "Simplicity",
    description: "To live life simply, with minimal needs",
  },
  {
    title: "Tradition",
    description: "To follow respected patterns of the past",
  },
  {
    title: "Curiosity",
    description: "To be curious and discover new things",
  },
  {
    title: "Rationality",
    description: "To be guided by reason and logic",
  },
  {
    title: "Spirituality",
    description: "To grow and mature spiritually",
  },
  {
    title: "Commitment",
    description: "To make enduring, meaningful commitments",
  },
  {
    title: "Responsibility",
    description:
      "To make and carry out responsible decisions and meeting my obligations",
  },
  {
    title: "Leisure",
    description: "To take time to relax and enjoy",
  },
  {
    title: "Genuineness",
    description: "To act in a manner that is true to who I am",
  },
  {
    title: "Romance",
    description: "To have intense, exciting love in my life",
  },
  {
    title: "Fun",
    description: "To play and have fun",
  },
  {
    title: "Non-Conformity",
    description: "To question and challenge authority and norms",
  },
  {
    title: "Connection",
    description: "To have close, supportive relationships with others",
  },
  {
    title: "Intimacy",
    description:
      "To fully know and be known by others close to me",
  },
  {
    title: "Respect",
    description: "Being respectful to others",
  },
  {
    title: "Compassion",
    description: "To feel and act on concern for others",
  },
  {
    title: "Justice",
    description: "To promote fair and equal treatment for all",
  },
  {
    title: "Contribution",
    description:
      "Having a sense of accomplishment and to make a lasting contribution in the world",
  },
  {
    title: "Generosity",
    description: "To give what I have to others",
  },
  {
    title: "Peace",
    description: "To work to promote peace in the world",
  },
  {
    title: "Helpfulness",
    description: "To be helpful to others",
  },
  {
    title: "Problem Solving ",
    description: "Figuring things out, solving problems",
  },
  {
    title: "Piety",
    description: "Acting consistently with my religious faith and beliefs",
  },
  {
    title: "Community",
    description: "Being a part of and contributing to a group",
  },
  {
    title: "Security",
    description:
      "Maintaining the safety and security of myself and my loved ones",
  },
  {
    title: "Loving",
    description:
      "Having relationships involving tenderness, love, and affection",
  },
  {
    title: "Competition",
    description: "Competing with others",
  },
  {
    title: "Creation",
    description: "Building, designing, and/or repairing things",
  },
  {
    title: "Loyalty",
    description: "Being loyal to friends, family , and/or my group",
  },
  {
    title: "Competent",
    description: "Being competent and effective in what I do",
  },
  {
    title: "Authority",
    description: "Having authority and being in charge of others",
  },
  {
    title: "Courageous",
    description: "Acting with courage",
  },
];
