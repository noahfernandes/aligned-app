import { createRouter, createWebHashHistory } from "vue-router";

import About from "@/views/About.vue";
import Login from "@/views/Login";
import Profile from "@/views/Profile";
import AlignmentsPage from "@/views/AlignmentsPage";
import Align from "@/views/Align";
import Discover from "@/views/Discover";
import store from "@/store";
import { AuthenticationModuleGetters } from "@/store/AuthenticationModule";
import CoreValuesPage from "@/views/CoreValuesPage";

export const RouteName = {
  Login: "login",
  Profile: "profile",
  Dashboard: "dashboard",
  Align: "align",
  CoreValues: "core-values",
  Discover: "discover",
  About: "about",
};

const routes = [
  {
    path: "/login",
    name: RouteName.Login,
    component: Login,
  },
  {
    path: "/profile",
    name: RouteName.Profile,
    component: Profile,
  },
  {
    path: "/dashboard",
    name: RouteName.Dashboard,
    component: AlignmentsPage,
  },
  {
    path: "/align",
    name: RouteName.Align,
    component: Align,
  },
  {
    path: "/core-values",
    name: RouteName.CoreValues,
    component: CoreValuesPage,
  },
  {
    path: "/discover",
    name: RouteName.Discover,
    component: Discover,
  },
  {
    path: "/about",
    name: RouteName.About,
    component: About,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const user = store.getters[AuthenticationModuleGetters.User];

  if (!user && to.name !== RouteName.Login) {
    next({ name: RouteName.Login });
  } else if (user && to.name === RouteName.Login) {
    next({ name: RouteName.Dashboard });
  } else {
    next();
  }
});

export default router;
